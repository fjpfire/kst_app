
const bpmUtils = {
	
	// 转到流转追踪
	navTrace(vm, queryString) {
		vm.$u.api.index().then(res => {
			if (res.result != 'true'){
				vm.$u.toast(res.message);
				return;
			}
			let url = vm.vuex_config.baseUrl + vm.vuex_config.adminPath
				+ '/bpm/bpmRuntime/trace?' + queryString + '&app=true&__sid='+vm.vuex_token;
			url = '/pages/common/webview?title=流程追踪&url=' + encodeURIComponent(url);
			uni.navigateTo({ url: url });
		});
	},
	
	// 转到流程表单
	navForm(vm, res) {
		if (res.result != 'true'){
			vm.$u.toast(res.message);
			return;
		}
		let buildFormUrl = (url) => {
			if (!(url && url != '')){
				vm.$u.toast('未设置表单地址');
				return;
			}
			let config = vm.vuex_config;
			let ctx = config.baseUrl + config.adminPath;
			let idx = ctx.indexOf('://');
			if (idx != -1){
				ctx = ctx.substring(idx+3);
			}
			idx = ctx.indexOf('/');
			if (idx != -1){
				ctx = ctx.substring(idx);
			}else{
				ctx = '';
			}
			if (ctx.length > 0 && url.substring(0, ctx.length) == ctx){
				return url.substring(ctx.length);
			}
			return url;
		}
		let url = res.mobileUrl;
		// 如果手机表单未设置，则获取PC表单地址
		if (!(url && url != '') && (res.pcUrl && res.pcUrl != '')){
			url = '/pages' + buildFormUrl(res.pcUrl);
			console.log('mobileUrl is not set, used pcUrl: ' + url);
		}else{
			url = buildFormUrl(url);
			console.log('mobileUrl: ' + url);
		}
		uni.navigateTo({ url: url });
	}
	
}

export default bpmUtils;