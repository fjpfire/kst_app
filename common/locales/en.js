/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
export default {
	common: {
		title: 'JeeSite',
	},
	nav: {
		home: 'Echar',
		user: 'User'
	},
	login: {
		title: 'Login',
		placeholderAccount: 'Enter Account',
		placeholderPassword: 'Enter Password',
		autoLogin: 'Auto Login',
		loginButton: 'Login',
		logoutButton: 'Logout',
		forget: 'Forget Password',
		reg: 'Resister Account',
		noLogin: 'No Login'
	},
	home: {
		title: 'Echar'
	},
	user: {
		title: 'User'
	}
}