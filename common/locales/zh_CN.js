/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
export default {
	common: {
		title: '出入库系统',
	},
	nav: {
		home: '日常业务',
		user: '我的',
	},
	login: {
		title: '登录',
		placeholderAccount: '请输入账号',
		placeholderPassword: '请输入密码',
		autoLogin: '自动登录',
		loginButton: '登录',
		logoutButton: '退出登录',
		forget: '忘记密码',
		reg: '注册账号',
		noLogin: '未登录'
	},
	home: {
		title: '日常业务'
	},
	user: {
		title: '我的'
	}
}