/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
const config = {
	
	// 产品名称
	productName: '科斯特仓库平台',
	
	// 公司名称
	companyName: '重庆轻企信息技术有限公司',
	
	// 产品版本号
	productVersion: '1.1.0',
	
	// 版本检查标识
	appCode: 'android',
	
	// 内部版本号码
	appVersion: 1,
	
	// 管理基础路径
	adminPath: '/a',
	
}

// 设置后台接口服务的基础地址 
config.baseUrl = 'http://192.168.1.22:8980/KST';
// config.baseUrl = 'http://127.0.0.1:8981/ASD';
// config.baseUrl = 'http://192.168.128.7:8981/ASD';
// 建议：打开下面注释，方便根据环境，自动设定服务地址
if (process.env.NODE_ENV === 'development'){
	// config.baseUrl = '/../js'; // 代理模式 vue.config.js 中找到 devServer 设置的地址
	// config.baseUrl = 'http://149q55n420.iok.la:47437/srm';
	// config.baseUrl = 'http://192.168.128.7:8981/ASD';
	config.baseUrl = 'http://192.168.1.22:8980/KST';
}

export default config;