module.exports = {
	//区域列表
	areaList: [{
		value: 'RESERVED',
		name: 'RESERVED'
	}, {
		value: 'EPC',
		name: 'EPC'
	}, {
		value: 'TID',
		name: 'TID'
	}, {
		value: 'USER',
		name: 'USER'
	}],
	//功率列表
	powerList: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17',
		'18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33'
	],
	frequencyList: ['RG_中国', 'RG_美国', 'RG_无', 'RG_韩国', 'RG_欧洲', 'RG_欧洲2', 'RG_欧洲3']
}
