/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作
const install = (Vue, vm) => {
	
	// 参数配置对象
	const config = vm.vuex_config;
	
	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		
		// 基础服务：登录登出、身份信息、菜单授权、切换系统、字典数据等
		lang: (params = {}) => vm.$u.get('/lang/'+params.lang),
		index: (params = {}) => vm.$u.get(config.adminPath+'/index', params),
		login: (params = {}) => vm.$u.post(config.adminPath+'/login', params),
		logout: (params = {}) => vm.$u.get(config.adminPath+'/logout', params),
		authInfo: (params = {}) => vm.$u.get(config.adminPath+'/authInfo', params),
		menuTree: (params = {}) => vm.$u.get(config.adminPath+'/menuTree', params),
		switchSys: (params = {}) => vm.$u.get(config.adminPath+'/switch/'+params.sysCode),
		dictData: (params = {}) => vm.$u.get(config.adminPath+'/sys/dictData/treeData', params),
		
		// 账号服务：验证码接口、忘记密码接口、注册账号接口等
		validCode: (params = {}) => vm.$u.getText('/validCode', params),
		getFpValidCode: (params = {}) => vm.$u.post('/account/getFpValidCode', params),
		savePwdByValidCode: (params = {}) => vm.$u.post('/account/savePwdByValidCode', params),
		getRegValidCode: (params = {}) => vm.$u.post('/account/getRegValidCode', params),
		saveRegByValidCode: (params = {}) => vm.$u.post('/account/saveRegByValidCode', params),
		
		// APP公共服务
		upgradeCheck: () => vm.$u.post('/app/upgrade/check', {appCode: config.appCode, appVersion: config.appVersion}),
		commentSave: (params = {}) => vm.$u.post('/app/comment/save', params),
		
		//条码
		tm:{
			// 产成品入库
			getWarehouse: (params = {}) => vm.$u.post(config.adminPath+'/Warehouse/GetWarehouse', params),
			getProcessflowBarCode: (params = {}) => vm.$u.post(config.adminPath+'/Rdrecord10/GetProcessflowBarCode', params),
			getPosition: (params = {}) => vm.$u.post(config.adminPath+'/Position/GetPosition', params),
			setRdrecord10Add: (params = {}) => vm.$u.postJson(config.adminPath+'/Rdrecord10/setRdrecord10Add', params),
			// 货位调整
			getBarCode:(params = {}) => vm.$u.postJson(config.adminPath+'/TransVouch/GetBarCode', params),
			setAdjustPVouchAdd: (params = {}) => vm.$u.postJson(config.adminPath+'/AdjustPVouch/setAdjustPVouchAdd', params),
			// 仓库调拨—调出 
			setTransVouchAdd: (params = {}) => vm.$u.postJson(config.adminPath+'/TransVouch/setTransVouchAdd', params),
			// 仓库调拨—调入
			getTransVouchtask: (params = {}) => vm.$u.post(config.adminPath+'/TransVouch/GetTransVouchtask', params),
			setTransVouchtaskAdd: (params = {}) => vm.$u.postJson(config.adminPath+'/TransVouch/setTransVouchtaskAdd', params),
		},
		
		// 个人信息修改
		user: {
			infoSaveBase: (params = {}) => vm.$u.post(config.adminPath+'/sys/user/infoSaveBase', params),
			infoSavePwd: (params = {}) => vm.$u.post(config.adminPath+'/sys/user/infoSavePwd', params),
			infoSavePqa: (params = {}) => vm.$u.post(config.adminPath+'/sys/user/infoSavePqa', params),
		},
		
		// 员工用户查询
		empUser: {
			listData: (params = {}) => vm.$u.get(config.adminPath+'/sys/empUser/listData', params),
		},
		
		// 组织机构查询
		office: {
			treeData: (params = {}) => vm.$u.get(config.adminPath+'/sys/office/treeData', params),
		},
		
		// 增删改查例子
		testData: {
			form: (params = {}) => vm.$u.post(config.adminPath+'/test/testData/form', params),
			list: (params = {}) => vm.$u.post(config.adminPath+'/test/testData/listData', params),
			save: (params = {}) => vm.$u.postJson(config.adminPath+'/test/testData/save', params),
			disable: (params = {}) => vm.$u.post(config.adminPath+'/test/testData/disable', params),
			enable: (params = {}) => vm.$u.post(config.adminPath+'/test/testData/enable', params),
			delete: (params = {}) => vm.$u.post(config.adminPath+'/test/testData/delete', params),
		},
		
		// 工作流引擎
		bpm: {
			myRuntimeList: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmMyRuntime/listData', params),
			myRuntimeForm: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmMyRuntime/form', params),
			myTaskList: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmMyTask/listData', params),
			myTaskForm: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmMyTask/form', params),
			getProcIns: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmRuntime/getProcIns', params),
			getTask: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmTask/getTask', params),
			stop: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmRuntime/stop', params),
			stopProcess: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmRuntime/stopProcess', params),
			claim: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmTask/claim', params),
			complete: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmTask/complete', params),
			turn: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmTask/turn', params),
			turnTask: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmTask/turnTask', params),
			back: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmTask/back', params),
			backTask: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmTask/backTask', params),
			move: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmTask/move', params),
			moveTask: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmTask/moveTask', params),
			rollback: (params = {}) => vm.$u.post(config.adminPath+'/bpm/bpmTask/rollback', params),
		},
		
		// 请假流程例子
		oa: {
			oaLeave: {
				form: (params = {}) => vm.$u.post(config.adminPath+'/foa/foaLeave/form', params),
				list: (params = {}) => vm.$u.post(config.adminPath+'/foa/foaLeave/listData', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/foa/foaLeave/save', params),
				delete: (params = {}) => vm.$u.post(config.adminPath+'/foa/foaLeave/delete', params),
			},
		},
		//btdm
		box:{
			get:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/getBoxData', params),
			getBoxQty:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/getBoxQty', params),
			save:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/save', params),
			findBoxMainList:(params = {}) => vm.$u.post(config.adminPath+'/btdm/btdmBoxMain/findBoxMainList', params),
			findRdrList:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/findRdrList', params),
			findRdrThList:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/findRdrThList', params),
			findRdrsThInum:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/findRdrsThInum', params),
			getBoxThOrPro:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/getBoxThOrPro', params),
			saveth:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/saveth', params),
			getBoxOrPro:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/getBoxOrPro', params),
			saveRdr:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/saveRdr', params),
			findRdrsInum:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/findRdrsInum', params),
			getCast:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/getCast', params),
			addCast:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/addCast', params),
			saveError:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/saveError', params),
			findRd09rList:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/findRd09rList', params),
			findRdrs09Inum:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/findRdrs09Inum', params),
			saveRdr09:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/saveRdr09', params),
			findQtyList:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/findQtyList', params),
			findBoxCodeList:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/findBoxCodeList', params),
			findrkQtyList:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/findrkQtyList', params),
			findscQtyList:(params = {}) => vm.$u.post(config.adminPath+'/btdm/api/box/findscQtyList', params),
		},
		basWarehouse:{
			treeData: (params = {}) => vm.$u.get(config.adminPath+'/bas/basWarehouse/treeData', params),
		},
		
		//爱思帝接口
		asdApi:{
			//上架
			findTmSl: (params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/findTmSl', params),
			checkSlByCode: (params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/checkSlByCode', params),
			upDataToU8: (params = {}) => vm.$u.postJson(config.adminPath+'/asd/Api/v1/upDataToU8', params), //上架
			
			//下架
			findTmLl: (params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/findTmLl', params), 
			getLl: (params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/getLl', params), 
			checkLlByCode: (params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/checkLlByCode', params), 
			downToU8: (params = {}) => vm.$u.postJson(config.adminPath+'/asd/Api/v1/downToU8', params), 
			
			//退料
			getXppData:(params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/getXppData', params), 
			saveTl:(params = {}) => vm.$u.postJson(config.adminPath+'/asd/Api/v1/saveTl', params), 
			findTlList:(params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/findTlList', params), 
			cheackTlXpp:(params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/cheackTlXpp', params), 
			upDataTlToU8:(params = {}) => vm.$u.postJson(config.adminPath+'/asd/Api/v1/upDataTlToU8', params), 
		},
		
		//基础档案
		basApi:{
			findPosTop:(params = {}) => vm.$u.post(config.adminPath+'/bas/api/v1/findPosTop', params),
			findPosRowByOne:(params = {}) => vm.$u.post(config.adminPath+'/bas/api/v1/findPosRowByOne', params),
			findPosEndByOne:(params = {}) => vm.$u.post(config.adminPath+'/bas/api/v1/findPosEndByOne', params),
		},
		invApi:{
			findBasInv:(params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/findBasInv', params),
			getHwCl:(params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/getHwCl', params),
		},
		qt:{
			saveUpData:(params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/saveUpData', params),
			getXjXpp:(params = {}) => vm.$u.post(config.adminPath+'/asd/Api/v1/getXjXpp', params),
			saveOutToU8:(params = {}) => vm.$u.postJson(config.adminPath+'/asd/Api/v1/saveOutToU8', params),
		},
		
	};
	
}

export default {
	install
}