/* 格式化时间
 * @method fmtDate
 * @param {String} str 时间戳字符串
 * @param {String} d 日期格式，默认为时间格式,1年月日,2月日时分,3年月1号
 * @return {String} 返回格式化之后的日期
*/
const fmtDate=(str,d)=>{
    let dt=str?new Date(str):new Date();
    let year=dt.getFullYear();
    let month=fixed2(dt.getMonth()+1);
    let date=fixed2(dt.getDate());
    let hours=fixed2(dt.getHours());
    let minutes=fixed2(dt.getMinutes());
    let secounds=fixed2(dt.getSeconds());
    let times='';
    if(d==1){
      times= `${year}-${month}-${date}`;
    }else if(d==2){
      times= `${month}-${date} ${hours}:${minutes}`;
    }else if(d==3){
      times= `${year}-${month}-01`;
    }else{
      times= `${year}-${month}-${date} ${hours}:${minutes}:${secounds}`;
    }
    return times;
}
/* 获取时间年、月、日
 * @method fmtDate
 * @param {String} str 时间戳字符串
 * @param {String} d 日期格式，默认为时间格式,1年月日,2月日时分,3年月1号
 * @return {String} 返回格式化之后的日期
*/
const getDateYMD=(str,d)=>{
	let dt=str?new Date(str):new Date();
	if(d=='Y'){
		return dt.getFullYear();
	}else if(d=='M'){
		return fixed2(dt.getMonth()+1);
	}
	return fixed2(dt.getDate());
}

/* 保留两位小数
 * @method fixed2
 * @param {String} str 需要转换的字符串
 * @return {String} 返回两位小数
*/
const fixed2=(str)=>{
    return Number.parseInt(str)<10?'0'+str:str;
}
/* 保留两位小数
 * @method fixed2
 * @param {number} value 需要转换的字符串
 * @param {number} number 保留小数位数
 * @return {String} 返回两位小数
*/
const toFixed=(value,number)=>{
	if(value==null || value==undefined){
		return '';
	}
	return parseFloat(value).toFixed(number);
}
//金额-万
const fmtNumber=(value)=>{
	value=Number(value);
	if(!value){
		return '';
	}
	if(value==0){
		return value+'';
	}
	var rs=(value/10000).toFixed(2);
	return rs;
}
/**
 * 
 * @param {保留万元} value 
 */
const formatMoney=(value)=>{
	if (!value) return "";
	//转换为万
	return "￥"+value +"万";
}
/**
* 将一维的扁平数组转换为多层级对象
*list 一维数组，数组中每一个元素需包含id和pId两个属性
*tree 多层级树状结构
*/
const buildTree=(list,pidStr,idStr,childrenStr)=>{
    let listOjb = {}; // 用来储存{key: obj}格式的对象
    let treeList = []; // 用来储存最终树形结构数据的数组
    // 将数据变换成{key: obj}格式，方便下面处理数据
    for (let i = 0; i < list.length; i++) {
        listOjb[list[i][idStr]] = list[i];
    }
    for (let j = 0; j < list.length; j++) {
        // 判断父级是否存在
        let haveParent = listOjb[list[j][pidStr]];
        if (haveParent) {
            // 如果有没有父级children字段，就创建一个children字段
            ! haveParent[childrenStr] && (haveParent[childrenStr] = []);
            // 在父级里插入子项
            haveParent[childrenStr].push(list[j]);
        } else {
            // 如果没有父级直接插入到最外层
            treeList.push(list[j]);
        }
    }
    return treeList;
}
/**
 * 获取JS转Base64之后的data类型
 * 例如:txt类型返回data:text/plain;base64,
 * @param suffix string 
 * @returns 
 */
const base64Type=(suffix)=>{
    console.log('文件后缀',suffix)
    switch (suffix) {
        case 'txt': return 'data:text/plain;base64,';
        case 'doc': return 'data:application/msword;base64,';
        case 'docx': return 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
        case 'xls': return 'data:application/vnd.ms-excel;base64,';
        case 'xlsx': return 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        case 'pdf': return 'data:application/pdf;base64,';
        case 'pptx': return 'data:application/vnd.openxmlformats-officedocument.presentationml.presentation;base64,';
        case 'ppt': return 'data:application/vnd.ms-powerpoint;base64,';
        case 'png': return 'data:image/png;base64,';
        case 'jpg': return 'data:image/jpeg;base64,';
        case 'gif': return 'data:image/gif;base64,';
        case 'svg': return 'data:image/svg+xml;base64,';
        case 'ico': return 'data:image/x-icon;base64,';
        case 'bmp': return 'data:image/bmp;base64,';
    }
    //未收集的类型返回空串
    return ''
}
/**
 * 返回base64MineType
 * @param base64Type data:text/plain;base64,
 * @returns text/plain
 */
const base64MineType=(base64Type)=>{
    console.log('base64Type',base64Type)
    let arr = base64Type.match(/data:(.*);base64,/)
    if(arr){
        return arr[1]
    }
}
/**
 * 获取文件后缀
 * @param fileName 
 * @returns
 */
const fileSuffix=(fileName)=>{
    console.log('文件名',fileName)
    var index1 = fileName.lastIndexOf(".");
    var index2 = fileName.length;
    var suffix = fileName.substring(index1 + 1, index2);
    return suffix
}

export default {
	fmtDate,
	getDateYMD,
	formatMoney,
	buildTree,
	base64MineType,
	fileSuffix,
	base64Type,
	toFixed,
	fmtNumber
}